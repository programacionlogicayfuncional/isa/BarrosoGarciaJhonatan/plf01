(ns plf01.core)

; <
(defn función-<-1 [a] (< a))

(defn función-<-2 [a b] (< a b))

(defn función-<-3 [a b c d e]
  (< a b c d e))

(función-<-1 10)
(función-<-2 15 30)
(función-<-3 20 5 14 30 5)


; <=
(defn función-<=-1 [a] (<= a))

(defn función-<=-2 [a b] (<= a b))

(defn función-<=-3 [a b c d e]
  (<= a b c d e))

(función-<=-1 65)
(función-<=-2 102 400)
(función-<=-3 20 20 20 20 20)


; ==
(defn función-==-1 [a] (== a))

(defn función-==-2 [a b] (== a b))

(defn función-==-3 [a b c]
  (== a b c))

(función-==-1 10)
(función-==-2 30 30)
(función-==-3 20 15 40)


; >
(defn función->-1 [a] (> a))

(defn función->-2 [a b] (> a b))

(defn función->-3 [a b c d]
  (> a b c d))

(función->-1 20)
(función->-2 60 42)
(función->-3 6 5 4 3)


; >=
(defn función->=-1 [a] (>= a))

(defn función->=-2 [a b] (>= a b))

(defn función->=-3 [a b c d e]
  (>= a b c d e))

(función->=-1 100)
(función->=-2 45 30)
(función->=-3 20 20 20 20 20)


; assoc
(defn función-assoc-1 [a b c] 
  (assoc a b c))

(defn función-assoc-2 [a b c d e] (assoc a b c d e))

(defn función-assoc-3 [a b c d e f g]
  (assoc a b c d e f g))

(función-assoc-1 [8 9 10] 0 7)
(función-assoc-2 [20 30 40] 0 5 3 40)
(función-assoc-3 [20 5 14 30 5] 0 '(1 3 4 7) 4 35 2 [11 16 20])


; assoc-in
(defn función-assoc-in-1 [a b c]
  (assoc-in a b c))

(defn función-assoc-in-2 [a b c] (assoc-in a b c))

(defn función-assoc-in-3 [a b c]
  (assoc-in a b c))

(función-assoc-in-1 [{:nombre "Pedro" :edad 15} {:nombre "Jorge" :edad 21}] [1 :nombre] "Jorge Octavio")
(función-assoc-in-2 [[10 20][30 40]] [0 0] '(5 8 9 10))
(función-assoc-in-3 [[20 5 14 30 5] [9 10]] [1 0] 30)


; concat
(defn función-concat-1 []
  (concat))

(defn función-concat-2 [a b] (concat a b))

(defn función-concat-3 [a b c d]
  (concat a b c d))

(función-concat-1)
(función-concat-2 [] '(9 8 7 4 ))
(función-concat-3 [\b \c] "def" "ghi" "jk")



; conj
(defn función-conj-1 [coll x]
  (conj coll x))

(defn función-conj-2 [coll x y] (conj coll x y))

(defn función-conj-3 [coll x y z]
  (conj coll x y z))

(función-conj-1 [7 8 9 10] 11)
(función-conj-2 {:nombre "Pedro" :edad 15} {:nacionalidad "Mexicano" :domicilio "-"} {:apellidoP "Cruz" :apellidoM " "})
(función-conj-3 '(\a \b \c) "def" "ghi" [\j \k])


; cons
(defn función-cons-1 [val secuencia]
  (cons val secuencia))

(defn función-cons-2 [val secuencia] (cons val secuencia))

(defn función-cons-3 [val secuencia]
  (cons val secuencia))

(función-cons-1 1 [7 8 9 10])
(función-cons-2 '(7 8) '(10 12 14 16))
(función-cons-3 [9 10 11] [12 13 14 15])

; contains?
(defn función-contains?-1 [coll key]
  (contains? coll key))

(defn función-contains?-2 [coll key]
  (contains? coll key))

(defn función-contains?-3 [coll key]
  (contains? coll key))

(función-contains?-1 [7 8 9 10] 0)
(función-contains?-2 {:nombre "Jorge Octavio" :edad 21} :nombre)
(función-contains?-3 #{0 [1 2 3] 4 5 6} 0)


; count
(defn función-count-1 [coll]
  (count coll))

(defn función-count-2 [coll]
  (count coll))

(defn función-count-3 [coll]
  (count coll))

(función-count-1 [7 8 9 10])
(función-count-2 {:nombre "Jorge Octavio" :apellidoP "Cruz" :edad 21})
(función-count-3 #{0 [1 2 3] 4 5 6})

;disj
(defn función-disj-1 [set]
  (disj set))

(defn función-disj-2 [set key]
  (disj set key))

(defn función-disj-3 [set k1 k2 k3]
  (disj set k1 k2 k3))

(función-disj-1 #{7 8 9 10})
(función-disj-2 #{20 60 70 80 90 100} 100)
(función-disj-3 #{0 [1 2 3] 4 5 6 '(9 10 11 12 13)} [1 2 3] 4 6)


;dissoc
(defn función-dissoc-1 [map]
  (dissoc map))

(defn función-dissoc-2 [map key]
  (dissoc map key))

(defn función-dissoc-3 [map k1 k2]
  (dissoc map k1 k2))

(función-dissoc-1 {:nombre "Alonso" :edad 21})
(función-dissoc-2 {:nombre "Pedro" :apellidoP "Garcia" :edad 21} :nombre)
(función-dissoc-3 {:nombre "Pedro" :apellidoP "Garcia" :edad 21} :apellidoP :edad)



;distinct
(defn función-distinct-1 []
  (distinct))

(defn función-distinct-2 [coll]
  (distinct coll))

(defn función-distinct-3 [coll]
  (distinct coll))

(función-distinct-1)
(función-distinct-2 [1 2 3 4 7 8 8 8])
(función-distinct-3 '(1 2 2 7 7 7 7 8 8 9 9 9))


;distinct?
(defn función-distinct?-1 [x]
  (distinct? x))

(defn función-distinct?-2 [x y]
  (distinct? x y))

(defn función-distinct?-3 [x y z]
  (distinct? x y z))

(función-distinct?-1 8)
(función-distinct?-2 8 9)
(función-distinct?-3 50 70 50)



;drop-last
(defn función-drop-last-1 [coll]
  (drop-last coll))

(defn función-drop-last-2 [num coll]
  (drop-last num coll))

(defn función-drop-last-3 [coll num]
  (drop-last num coll))

(función-drop-last-1 #{20 30 40 50 [60 70 80] 90 '(100 110)})
(función-drop-last-2 2 [1 7 9 10 12 14 17 [19 21] 34])
(función-drop-last-3 {:nombre "Alonso" :apellidoP "Garcia" :edad 21 :domicilio {:calle "Macedonio" :num 27} :estado-civil "soltero"} 1)


;empty
(defn función-empty-1 [coll]
  (empty coll))

(defn función-empty-2 [coll]
  (empty coll))

(defn función-empty-3 [coll]
  (empty coll))

(función-empty-1 #{20 30 40 50 [60 70 80] 90 '(100 110)})
(función-empty-2  [1 7 9 10 12 14 17 [19 21] 34])
(función-empty-3 {:nombre "Alonso" :apellidoP "Garcia" :edad 21 :domicilio {:calle "Macedonio" :num 27} :estado-civil "soltero"})

;empty?
(defn función-empty?-1 [coll]
  (empty? coll))

(defn función-empty?-2 [coll]
  (empty? coll))

(defn función-empty?-3 [coll]
  (empty? coll))

(función-empty?-1 #{20 30 40 50 [60 70 80] 90 '(100 110)})
(función-empty?-2  '())
(función-empty?-3 {:nombre "Alonso" :apellidoP "Garcia" :edad 21 :domicilio {:calle "Macedonio" :num 27} :estado-civil "soltero"})

;even?
(defn función-even?-1 [num]
  (even? num))

(defn función-even?-2 [num]
  (even? num))

(defn función-even?-3 [num]
  (even? num))

(función-even?-1 79)
(función-even?-2 91)
(función-even?-3 128)


;false?
(defn función-false?-1 [x]
  (false? x))

(defn función-false?-2 [x]
  (false? x))

(defn función-false?-3 [x]
  (false? x))

(función-false?-1 true)
(función-false?-2 false)
(función-false?-3 128)


;find
(defn función-find-1 [map key]
  (find map key))

(defn función-find-2 [map key]
  (find map key))

(defn función-find-3 [map key]
  (find map key))

(función-find-1 {:nombre "Pedro" :edad 21} :nombre)
(función-find-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 21 :domicilio {:calle "Macedonio" :num 27} :estado-civil "soltero"} :domicilio)
(función-find-3 [10 20 30 70 90 110] 2)


;first
(defn función-first-1 [coll]
  (first coll))

(defn función-first-2 [coll]
  (first coll))

(defn función-first-3 [coll]
  (first coll))

(función-first-1 #{10 20 30 40 50 60})
(función-first-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 21 :domicilio {:calle "Macedonio" :num 27} :estado-civil "soltero"})
(función-first-3 [10 20 30 70 90 110])


;flatten
(defn función-flatten-1 [x]
  (flatten x))

(defn función-flatten-2 [x]
  (flatten x))

(defn función-flatten-3 [x]
  (flatten x))

(función-flatten-1 '(1 2 [3 (4 5)]))
(función-flatten-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 21 :domicilio {:calle "Macedonio" :num 27} :estado-civil "soltero"})
(función-flatten-3 [[1 5] 10 20 30 70 90 110])


;frequencies
(defn función-frequencies-1 [coll]
  (frequencies coll))

(defn función-frequencies-2 [coll]
  (frequencies coll))

(defn función-frequencies-3 [coll]
  (frequencies coll))

(función-frequencies-1 '(1 2 [3 (4 5)] [3 (4 5)] [3 (4 5)] 10 20 20))
(función-frequencies-2 [\a \b \c \d \e \A \b \C \d \e])
(función-frequencies-3 [[1 5] 10 20 30 70 90 110 10 20 30 10 20 30])

;get
(defn función-get-1 [map key]
  (get map key))

(defn función-get-2 [map key not-found]
  (get map key not-found))

(defn función-get-3 [map key]
  (get map key nil))

(función-get-1 {:nombre "Pedro" :edad 17} :nombre)
(función-get-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 24 :domicilio {:calle "Macedonio" :número "S/N"}} :domicilio nil)
(función-get-3 [[1 5] 10 20 30 70 90 110 10 20 30 10 20 30] 50)


;get-in
(defn función-get-in-1 [m ks]
  (get-in m ks))

(defn función-get-in-2 [m ks not-found]
  (get-in m ks not-found))

(defn función-get-in-3 [m ks]
  (get-in m ks nil))

(función-get-in-1 {:nombre "Pedro" :edad 17} #{:nombre})
(función-get-in-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 24 :domicilio {:calle "Macedonio Alcala" :número "S/N"}} [:domicilio :calle] nil)
(función-get-in-3 [[1 5] 10 20 30 70 90 110 10 20 30 10 20 30] '(0 1))


;into
(defn función-into-1 []
  (into))

(defn función-into-2 [to]
  (into to))

(defn función-into-3 [to from]
  (into to from))

(función-into-1)
(función-into-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 24 :domicilio {:calle "Macedonio Alcala" :número "S/N"}})
(función-into-3 #{} [[1 5] 10 20 30 70 90 110 10 20 30 10 20 30])


;key
(defn función-key-1 [m]
  (map key m))

(defn función-key-2 [m]
  (map key m))

(defn función-key-3 [m]
  (key m))

(función-key-1 {:a 10 :b 30 :c 40})
(función-key-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 24 :domicilio {:calle "Macedonio Alcala" :número "S/N"}})
(función-key-3 (clojure.lang.MapEntry. :a :10))


;keys
(defn función-keys-1 [map]
  (keys map))

(defn función-keys-2 [map]
  (keys map))

(defn función-keys-3 [map]
  (keys map))

(función-keys-1 {:a 10 :b 30 :c 40})
(función-keys-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 24 :domicilio {:calle "Macedonio Alcala" :número "S/N"}})
(función-keys-3 {:a 10 :b 20 :c 30 :d 40 :e 50 :f 60 :g 70})


;max
(defn función-max-1 [x]
  (max x))

(defn función-max-2 [x y]
  (max x y))

(defn función-max-3 [w x y z]
  (max w x y z))

(función-max-1 1000)
(función-max-2 -5 -30)
(función-max-3 0 1 -2 -4)


;merge
(defn función-merge-1 [a]
  (merge a))

(defn función-merge-2 [a b]
  (merge a b))

(defn función-merge-3 [a b c]
  (merge a b c))

(función-merge-1 {:a 10 :b 5})
(función-merge-2 {:a 10 :b 5} {:b 15 :c 25 :d 30 :f {:f1 14 :f2 17}})
(función-merge-3 {:x 1 :y 2} {:y 3 :z 4} {:w 7 :z 10})


;min
(defn función-min-1 [x]
  (min x))

(defn función-min-2 [x y]
  (min x y))

(defn función-min-3 [w x y z]
  (min w x y z))

(función-min-1 1000)
(función-min-2 -5 -30)
(función-min-3 0 1 -2 -4)


;neg?
(defn función-neg?-1 [num]
  (neg? num))

(defn función-neg?-2 [num]
  (neg? num))

(defn función-neg?-3 [num]
  (neg? num))

(función-neg?-1 1000)
(función-neg?-2 -30)
(función-neg?-3 -4)


;nil?
(defn función-nil?-1 [x]
  (nil? x))

(defn función-nil?-2 [x]
  (nil? x))

(defn función-nil?-3 [x]
  (nil? x))

(función-nil?-1 nil)
(función-nil?-2 -30)
(función-nil?-3 true)


;not-empty
(defn función-not-empty-1 [coll]
  (not-empty coll))

(defn función-not-empty-2 [coll]
  (not-empty coll))

(defn función-not-empty-3 [coll]
  (not-empty coll))

(función-not-empty-1 [])
(función-not-empty-2 '())
(función-not-empty-3 #{10 20 30 40 50})


;nth
(defn función-nth-1 [coll index]
  (nth coll index))

(defn función-nth-2 [coll index not-found]
  (nth coll index not-found))

(defn función-nth-3 [coll index]
  (nth coll index nil))

(función-nth-1 [11 21 31 41] 3)
(función-nth-2 '(10 20 30 [40 50 60] 89 74) 3 nil)
(función-nth-3 [10 20 30 #{70 8090}] 3)


;odd?
(defn función-odd?-1 [num]
  (odd? num))

(defn función-odd?-2 [num]
  (odd? num))

(defn función-odd?-3 [num]
  (odd? num))

(función-odd?-1 1000)
(función-odd?-2 5)
(función-odd?-3 -7)


;partition
(defn función-partition-1 [n coll]
  (partition n coll))

(defn función-partition-2 [n step coll]
  (partition n step coll))

(defn función-partition-3 [n step pad coll]
  (partition n step pad coll))

(función-partition-1 2 [1 2 3 4 5 6 7])
(función-partition-2 3 1 [1 2 3 4 5 6 7]) 
(función-partition-3 7 4 nil [1 2 3 4 5 6 7])


;partition-all
(defn función-partition-all-1 [n]
  (partition-all n))

(defn función-partition-all-2 [n coll]
  (partition-all n coll))

(defn función-partition-all-3 [n step coll]
  (partition-all n step coll))

(función-partition-all-1 5)
(función-partition-all-2 3 [1 2 3 4 5 6 7])
(función-partition-all-3 7 4 [1 2 3 4 5 6 7])


;peek
(defn función-peek-1 [coll]
  (peek coll))

(defn función-peek-2 [coll]
  (peek coll))

(defn función-peek-3 [coll]
  (peek coll))

(función-peek-1 [])
(función-peek-2 '(17 22 14 17))
(función-peek-3 ['(1 2 3 4 5 6 7) 9 10 15 30])


;pop
(defn función-pop-1 [coll]
  (pop coll))

(defn función-pop-2 [coll]
  (pop coll))

(defn función-pop-3 [coll]
  (pop coll))

(función-pop-1 [10 11 12 15 17 18 19 20 22 21 41 45 47 48 49])
(función-pop-2 '(1 7 18 19 20 25 70))
(función-pop-3 ['(7 8 9) [10 11 21] '(17 19 45)])



;pos?
(defn función-pos?-1 [num]
  (pos? num))

(defn función-pos?-2 [num]
  (pos? num))

(defn función-pos?-3 [num]
  (pos? num))

(función-pos?-1 1000)
(función-pos?-2 -30)
(función-pos?-3 -4)


;quot
(defn función-quot-1 [num div]
  (quot num div))

(defn función-quot-2 [num div]
  (quot num div))

(defn función-quot-3 [num div]
  (quot num div))

(función-quot-1 1000 50)
(función-quot-2 -30 4)
(función-quot-3 -4 -2)


;range
(defn función-range-1 [end]
  (range end))

(defn función-range-2 [start end]
  (range start end))

(defn función-range-3 [start end step]
  (range start end step))

(función-range-1 10)
(función-range-2 10 30)
(función-range-3 10 100 5)


;rem
(defn función-rem-1 [num div]
  (rem num div))

(defn función-rem-2 [num div]
  (rem num div))

(defn función-rem-3 [num div]
  (rem num div))

(función-rem-1 1000 50)
(función-rem-2 -30 4)
(función-rem-3 -174 -31)


;repeat
(defn función-repeat-1 [x]
  (repeat x))

(defn función-repeat-2 [x]
  (take 10 (repeat x)))

(defn función-repeat-3 [n x]
  (repeat n x))

(función-repeat-1 5)
(función-repeat-2 9)
(función-repeat-3 5 7)


;replace
(defn función-replace-1 [smap]
  (replace smap))

(defn función-replace-2 [smap coll]
  (replace smap coll))

(defn función-replace-3 [smap coll]
  (replace smap coll))

(función-replace-1 [10 11 12 15 17 18 19 20 22 21 41 45 47 48 49])
(función-replace-2 [1 7 18 19 20 25 70] [0 1 2 3])
(función-replace-3 {2 :two, 3 :three 4 :four 5 :five 6 :six} [4 2 3 4 5 6 2])


;rest
(defn función-rest-1 [coll]
  (rest coll))

(defn función-rest-2 [coll]
  (rest coll))

(defn función-rest-3 [coll]
  (rest coll))

(función-rest-1 [10 11 12 15 17 18 19 20 22 21 41 45 47 48 49])
(función-rest-2 '(1 7 18 19 20 25 70))
(función-rest-3 ['(7 8 9) [10 11 21] '(17 19 45)])


;select-keys
(defn función-select-keys-1 [map keyseq]
  (select-keys map keyseq))

(defn función-select-keys-2 [map keyseq]
  (select-keys map keyseq))

(defn función-select-keys-3 [map keyseq]
  (select-keys map keyseq))

(función-select-keys-1 [10 11 12 15 17 18 19 20 22 21 41 45 47 48 49] [0 1 2 2 5 5])
(función-select-keys-2 {:a 1 :b 2} [:a :c])
(función-select-keys-3 {:nombre "Jack" :apellidoP "Barahona" :edad 17} [:nombre :edad])


;shuffle
(defn función-shuffle-1 [coll]
  (shuffle coll))

(defn función-shuffle-2 [coll]
  (shuffle coll))

(defn función-shuffle-3 [coll]
  (shuffle coll))

(función-shuffle-1 [10 11 12 15 17 18 19 20 22 21 41 45 47 48 49])
(función-shuffle-2 '(1 7 18 19 20 25 70))
(función-shuffle-3 ['(7 8 9) [10 11 21] '(17 19 45)])


;sort
(defn función-sort-1 [coll]
  (sort coll))

(defn función-sort-2 [comp coll]
  (sort comp coll))

(defn función-sort-3 [comp coll]
  (sort comp coll))

(función-sort-1 [-1 2 8 4 3 5 19 78 14 7 10 22 54 55 88 88 74])
(función-sort-2 > '(1 17 24 98 14 75 45 20 30))
(función-sort-3 < [1 9 7 14 16 19 74 15 18 72 73 18 92 14 74])


;split-at
(defn función-split-at-1 [n coll]
  (split-at n coll))

(defn función-split-at-2 [n coll]
  (split-at n coll))

(defn función-split-at-3 [n coll]
  (split-at n coll))

(función-split-at-1 3 [-1 2 8 4 3 5 19 78 14 7 10 22 54 55 88 88 74])
(función-split-at-2 10 '(1 17 24 98 14 75 45 20 30))
(función-split-at-3 5 [1 9 7 14 16 19 74 15 18 72 73 18 92 14 74])


;str
(defn función-str-1 []
  (str))

(defn función-str-2 [x]
  (str x))

(defn función-str-3 [x y z]
  (str x y z))

(función-str-1)
(función-str-2 '(1 17 24 98 14 75 45 20 30))
(función-str-3 5 [1 9 7 14 16 19 74 15 18 72 73 18 92 14 74] \a)


;subs
(defn función-subs-1 [s start]
  (subs s start))

(defn función-subs-2 [s start end]
  (subs s start end))

(defn función-subs-3 [s start end]
  (subs s start end))

(función-subs-1 "hola, buenos días" 6)
(función-subs-2 "Anita lava la tina" 6 10)
(función-subs-3 "40 6 10" 0 1)


;subvec
(defn función-subvec-1 [v start]
  (subvec v start))

(defn función-subvec-2 [v start end]
  (subvec v start end))

(defn función-subvec-3 [v start end]
  (subvec v start end))

(función-subvec-1 [1 2 3 4 5 6 7 8 9 [21 41]] 3)
(función-subvec-2 [1 2 3 4 5 7 4] 2 5)
(función-subvec-3 [7 10 11 12 14 17] 1 4)


;take
(defn función-take-1 [n]
  (take n))

(defn función-take-2 [n coll]
  (take n coll))

(defn función-take-3 [n coll]
  (take n coll))

(función-take-1 5)
(función-take-2 5 [1 2 3 4 5 7 4 9 10 11 12 14])
(función-take-3 8 '(7 14 11 20 25 27 2 4 7 8 9 36 2 1 1 17))


;true?
(defn función-true?-1 [x]
  (true? x))

(defn función-true?-2 [x]
  (true? x))

(defn función-true?-3 [x]
  (true? x))

(función-true?-1 false)
(función-true?-2 true)
(función-true?-3 "hola")


;val
(defn función-val-1 [e]
  (val (first e)))

(defn función-val-2 [e]
  (map val e))

(defn función-val-3 [e]
  (val e))

(función-val-1 {:a 10 :b 30 :c 40})
(función-val-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 24 :domicilio {:calle "Macedonio Alcala" :número "S/N"}})
(función-val-3 (clojure.lang.MapEntry. :a :10))


;vals
(defn función-vals-1 [map]
  (vals map))

(defn función-vals-2 [map]
  (vals map))

(defn función-vals-3 [map]
  (vals map))

(función-vals-1 {:a 10 :b 30 :c 40})
(función-vals-2 {:nombre "Alonso" :apellidoP "Garcia" :edad 24 :domicilio {:calle "Macedonio Alcala" :número "S/N"}})
(función-vals-3 {:a 10 :b 20 :c 30 :d 40 :e 50 :f 60 :g 70})


;zero?
(defn función-zero?-1 [num]
  (zero? num))

(defn función-zero?-2 [num]
  (zero? num))

(defn función-zero?-3 [num]
  (zero? num))

(función-zero?-1 0)
(función-zero?-2 0.0)
(función-zero?-3 1.5787)


;zipmap
(defn función-zipmap-1 [keys vals]
  (zipmap keys vals))

(defn función-zipmap-2 [keys vals]
  (zipmap keys vals))

(defn función-zipmap-3 [keys vals]
  (zipmap keys vals))

(función-zipmap-1 [:a :b :c] [1 2 3 4])
(función-zipmap-2 [:a :b :c :d :e] [1 2 3 4 5])
(función-zipmap-3 [:html :body :div] (repeat {:margin 0 :padding 0}))